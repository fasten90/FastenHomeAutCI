Config names    | FUNCTION_DISPLAY_MENU | FUNCTION_GAME_SNAKE | FUNCTION_DISPLAY_SHOW_CLOCK | FUNCTION_DISPLAY_SHOW_SCREEN | FUNCTION_TRAFFIC_LIGHT | FUNCTION_CHARGER
With display    | x                     | x                   |                             |                              |                        |
With display 2  | x                     | x                   | x                           |                              |                        |
With display 3  | x                     | x                   | x                           | x                            |                        |
With display 4  | x                     | x                   | x                           | x                            | x                      |
With display 5  | x                     | x                   | x                           | x                            | x                      | x
With display 6  | x                     |                     |                             | x                            |                        |
Without display |                       |                     |                             |                              |                        |


